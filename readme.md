call 
> java -jar kobalt/wrapper/kobalt-wrapper.jar --profiles dev assemble

It does include resources from "src/dev/resources" but application.properties is not taken from "dev"  - it was taken from "src/main/resources"
Expected: resources from "dev" override resources from "main" 
Note: Resulting WAR contains "/WEB-INF/classes/dev.properties" (which is good)

Try to call
> java -jar kobalt/wrapper/kobalt-wrapper.jar --profiles dev assemble

Product class contains Product for "dev" version as expected

Then try to call (clean all files before build)
> java -jar kobalt/wrapper/kobalt-wrapper.jar --profiles pro assemble

Product class contains Product for "dev" and "dev.properties" as before
Expected: pro classes and resources should be included