import com.beust.kobalt.*
import com.beust.kobalt.api.*
import com.beust.kobalt.plugin.application.*
import com.beust.kobalt.plugin.packaging.*
import com.beust.kobalt.plugin.kotlin.*

val kotlinVersion = "1.0.0"

val pro = false
val dev = false

val p = project {

    name = "KobaltProjectProfilesExample"
    group = "com.example"
    artifactId = name
    version = "0.0.1"
    packageName = "com.example"

    dependencies {
        compile("org.jetbrains.kotlin:kotlin-stdlib:" + kotlinVersion)
        compile("org.jetbrains.kotlin:kotlin-reflect:" + kotlinVersion)

        provided("javax:javaee-api:7.0")
        provided("javax.servlet:javax.servlet-api:3.1.0")
    }

    assemble {
        war {}
    }


    sourceDirectories {
        if(pro) {
            path("src/pro/kotlin", "src/pro/resources")
        } else if (dev) {
            path("src/dev/kotlin", "src/dev/resources")
        }
        path("src/main/kotlin")
    }
}
