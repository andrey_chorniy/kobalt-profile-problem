package com.example;

import java.io.IOException;
import java.util.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebStarter implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("Product name: " + new Product().getAppName());
        Properties properties = new Properties();
        try {
            properties.load(WebStarter.class.getResourceAsStream("/application.properties"));
            System.out.println("Properties:"+properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
